package com.bestrada.pagination.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class PaginationDemo {
	public static void main(String[] args) {
		SpringApplication.run(PaginationDemo.class, args);
	}
}
