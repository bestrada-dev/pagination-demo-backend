package com.bestrada.pagination.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.bestrada.pagination.example.model.Article;
import com.bestrada.pagination.example.repository.ArticleRepository;

@RestController
public class ArticleController {
	 @Autowired  private ArticleRepository articleRepository;

	 @GetMapping("/articles")
	 public Page<Article> getAllArticles(Pageable pageable) {
	 	return articleRepository.findAll(pageable);
	 }
}
